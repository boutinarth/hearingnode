var db = require('../db');

var PointVente = {
    getpointsvente: function(callback)
    {
        return db.query('SELECT * from pointvente', callback);
    },
    createpointvente: function (PointVente, callback) {
        return db.query('Insert into pointvente(id_societe,nom,ville,pos_gps,adresse,concurrents) values(?, ?, ?, ?, ?, ?)',
        	[PointVente.id_societe,PointVente.nom,PointVente.ville,PointVente.pos_gps,PointVente.adresse,PointVente.concurrents], callback);
    }
}

module.exports = PointVente;