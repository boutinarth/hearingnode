var express = require('express');
var app = express();
var cors = require('cors');
app.use(cors());

var SocieteController = require('./societe/SocieteController');
app.use('/societe', SocieteController);

var PointVenteController = require('./point-vente/PointVenteController');
app.use('/point-vente',PointVenteController);

var QuestionnaireController = require('./questionnaire/QuestionnaireController');
app.use('/questionnaire',QuestionnaireController);

var QuestionController = require('./question/QuestionController');
app.use('/question',QuestionController);

module.exports = app;